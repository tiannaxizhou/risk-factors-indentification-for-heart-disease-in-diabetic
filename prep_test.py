import glob
import collections
from xml.dom import minidom
from nltk import word_tokenize
from nltk.stem.snowball import SnowballStemmer
from itertools import groupby

# preprocessing input test files
data_dict = {}
next = 26785
with open("./data/fit_data_encoder.txt", 'r') as f:
    for line in f:
        value = line.split(' ')[0]
        key = line.split(' ')[1].replace('\n', '')
        data_dict[key] = value

encoded_notes = []
files = glob.glob("./data/testing-RiskFactors-noTags/*.xml")
for file in files:
    dom = minidom.parse(file)
    # get raw texts
    notes = dom.getElementsByTagName('TEXT')[0].firstChild.nodeValue.lower()
    # tokenize
    notes = word_tokenize(notes)
    # stemming
    stemmer = SnowballStemmer("english")
    notes = [stemmer.stem(x) for x in notes]
    # encode text
    text = []
    for x in notes:
        try:
            text.append(data_dict[x])
        except KeyError:
            data_dict[x] = next
            text.append(next) # treat unknown words
            next += 1
    encoded_notes.append(text)
    print("Done with file ", file)

print("new length: ", len(data_dict))
# seperate sentences
period_at = 3
final_notes = []
for each in encoded_notes:
    text = [[]]
    id = 0
    for x in each:
        text[id].append(int(x))
        if int(x) == period_at:
            text.append([])
            id += 1
    final_notes.append(text)

# write new dictionary
file = open("./data/fit_data_encoder.txt", 'w')
for word, id in data_dict.items():
    file.write(str(id) + ' ' + str(word) + '\n')
file.close()


# write encoded data to file
def outfile(files, notes):
    path = "./test/"
    for i in range(len(notes)):
        file = open(path + files[i][-10:-4] + '.txt', 'w')
        for j in range(len(notes[i])):
            encoded_notes = ','.join(str(x) for x in notes[i][j])
            file.write(encoded_notes + '\n')
            #print(encoded_notes)
        file.close()

outfile(files, final_notes)
