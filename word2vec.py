import glob
from xml.dom import minidom
from gensim.models import Word2Vec
from nltk import word_tokenize, sent_tokenize
from nltk.stem.snowball import SnowballStemmer
import multiprocessing

# word2vec (dimension = 20/40/60)
sentences = []
dir = ["./data/training-RiskFactors-Complete/*.xml", "./data/testing-RiskFactors-noTags/*.xml"]
for d in dir:
    files = glob.glob(d)
    for file in files:
        dom = minidom.parse(file)
        notes = dom.getElementsByTagName("TEXT")[0].firstChild.nodeValue.lower()
        notes = sent_tokenize(notes)
        stemmer = SnowballStemmer("english")
        notes = [[stemmer.stem(i) for i in word_tokenize(x)] for x in notes]
        sentences += notes

cores = multiprocessing.cpu_count()
model = Word2Vec(sentences = sentences, size = 60, min_count = 1, window = 5, workers = cores)
model.save('./word2vec_60.model')
