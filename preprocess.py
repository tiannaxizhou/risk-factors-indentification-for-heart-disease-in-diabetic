import glob
import collections
from xml.dom import minidom
from nltk import word_tokenize
from nltk.stem.snowball import SnowballStemmer
from itertools import groupby
from gensim.models import Word2Vec
from nltk import sent_tokenize
import multiprocessing


def get_annotation(element, attr):
    text = element.getAttribute('text').strip().lower()
    tag = element.tagName
    attr = element.getAttribute(attr).lower().strip().replace(' ','_')

    if tag == 'SMOKER' or tag == 'FAMILY_HIST':
        return(text, tag.strip().lower() + '&' + attr)
    else:
        time = element.getAttribute('time').lower().strip().replace(' ', '_')
        return (text, tag.strip().lower() + '&' + attr, time)

def tokenize(annotations):
    results = []
    tokens = []
    continuing = []
    for annotation in annotations:
        if len(annotation) == 3:
            continuing.append(annotation)
    for annotation in annotations:
        if len(annotation) == 3:
            if (annotation[0], annotation[1], 'before_dct') in continuing and (annotation[0], annotation[1], 'during_dct') in continuing and (annotation[0], annotation[1], 'after_dct') in continuing:
                results.append((annotation[0], annotation[1] + '&continuing'))
            else:
                results.append((annotation[0], annotation[1] + '&' + annotation[2]))
        else:
            results.append(annotation)
    results = list(set(results))
    for result in results:
        token = word_tokenize(result[0])
        tokens.append((token, result[1]))
    return tokens


# preprocessing input files
data, label, data_counter, label_counter, data_list, label_list = [], [], [], [], [], []
tags = ['CAD', 'DIABETES', 'FAMILY_HIST', 'HYPERLIPIDEMIA', 'HYPERTENSION', 'MEDICATION', 'OBESE', 'SMOKER']
files = glob.glob("./data/training-RiskFactors-Complete/*.xml")

for file in files:
    dom = minidom.parse(file)
    elements = [dom.getElementsByTagName(tag) for tag in tags]
    annotations = []
    for element in elements: # for each tag
        annotation = []
        for ele in element: # for each doc
            anno = []
            for e in ele.getElementsByTagName(ele.tagName): # for each annotator
                if e.tagName == 'MEDICATION':
                    anno.append(get_annotation(e, 'type1'))
                elif e.tagName == 'SMOKER':
                    anno.append(get_annotation(e, 'status'))
                else:
                    anno.append(get_annotation(e, 'indicator'))
                annotation.append(anno)
        if annotation:
            annotations.append(annotation)

    # list of annotations for each file [(text, tag&indicator, [time])...]
    annotations = list(set([y for x in [y for x in annotations for y in x] for y in x]))
    # filter
    annotations = [x for x in annotations if x[1] != 'family_hist&not_present' and x[1] != 'smoker&unknown']
    # tokenize
    annotations = tokenize(annotations)
    annotations.sort(key = lambda x : len(x[0]), reverse = True)
    #print(annotations)

    # get raw texts
    notes = dom.getElementsByTagName('TEXT')[0].firstChild.nodeValue.lower()
    notes = word_tokenize(notes)
    #print(notes)

    # find position of the evidence in text
    indices = []
    for annotation in annotations:
        text = annotation[0]
        index = 0
        pos = []
        for i in range(len(notes)):
            if notes[i] == text[0]:
                start = index
                end = start
                flag = True
                if (i + len(text)) > len(notes): # deal with eof
                    continue
                for j in range(1, len(text)):
                    if notes[i + j] == text[j]:
                        end += 1
                    else:
                        end = start
                        flag = False
                        break
                if flag:
                    pos.append((start, end))
            index += 1
        if pos:
            indices.append(pos)
    #print(indices)

    # BIO tagging for raw text: O for others, I-indicator for indicators w/ time attribute
    chunktags = ['O' for x in notes]
    for i in range(len(indices)):
        for j in range(len(indices[i])):
            start = indices[i][j][0]
            end = indices[i][j][1]
            for k in range(start, end + 1):
                chunktags[k] = 'I-' + annotations[i][1]


    # stemming
    stemmer = SnowballStemmer("english")
    notes = [stemmer.stem(x) for x in notes]
    # update global variables
    data += notes
    label += chunktags
    data_list.append(notes)
    label_list.append(chunktags)
    print("Done with file ", file)

# write encoder to file
print("start")
data_counter = collections.Counter(data).most_common()
label_counter = collections.Counter(label).most_common()
#print(data_counter, label_counter)


period_at = -1
data_fit_y, label_fit_y = [], []
file = open("./data/fit_data_encoder.txt", 'w')
for i, d in enumerate(data_counter):
    data_fit_y.append(d[0])
    if d[0] == '.':
        period_at = i + 2
    file.write(str(i + 2) + ' ' + d[0] + '\n')
file.close()

file = open("./data/fit_label_encoder.txt", 'w')
for i, l in enumerate(label_counter):
    label_fit_y.append(l[0])
    file.write(str(i + 1) + ' ' + l[0] + '\n')
file.close()

print("encoding")

# encoder -- takes about three hours
encoded_data = []
file = open("./data/data_encoded.txt", 'w')
for data in data_list:
    each = []
    for x in data:
        for i, d in enumerate(data_counter):
            if x == d[0]:
                each.append(i + 2)
    encoded_data.append(each)
    file.write(','.join(str(x) for x in each) + '\n')
print("data encoded")
file.close()

file = open("./data/label_encoded.txt", 'w')
encoded_label = []
for label in label_list:
    each = []
    for x in label:
        for i, l in enumerate(label_counter):
            if x == l[0]:
                each.append(i + 1)
    encoded_label.append(each)
    file.write(','.join(str(x) for x in each) + '\n')
print("label encoded")
file.close()


# seperate sentences
periods_at = [[i for i, x in enumerate(ed) if x == period_at] for ed in encoded_data]
encoded_data = [[list(g) for k, g in groupby(iterable, lambda x: x in (period_at,)) if not k] for iterable in encoded_data]
for i in range(len(periods_at)):
    for j in range(len(periods_at[i])):
        id = periods_at[i][j]
        encoded_label[i][id] = -1
encoded_label = [[list(g) for k, g in groupby(iterable, lambda x: x in (-1,)) if not k] for iterable in encoded_label]

# write encoded data to file
def outfile(files, data, label):
    path = "./train/"
    for i in range(len(data)):
        file = open(path + files[i][-10:-4] + '.txt', 'w')
        for j in range(len(data[i])):
            encoded_data = ','.join(str(x) for x in data[i][j])
            encoded_label = ','.join(str(x) for x in label[i][j])
            file.write(encoded_data + ' ' + encoded_label + '\n')
            print(encoded_data + '\t' + encoded_label + '\n')
        file.close()

outfile(files, encoded_data, encoded_label)
