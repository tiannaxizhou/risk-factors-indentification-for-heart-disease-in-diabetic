import glob
import numpy as np
import gensim
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers.embeddings import Embedding
from keras.layers import SimpleRNN
from keras.layers.wrappers import TimeDistributed
from keras.layers import Dense
from keras.optimizers import Nadam, RMSprop
from keras.models import model_from_json
from keras.preprocessing import sequence
from sklearn.preprocessing import LabelEncoder

'''
TRAIN
'''
data_dict, label_dict = {}, {}
with open("./data/fit_data_encoder.txt", 'r') as f:
    for line in f:
        key = line.split(' ')[0]
        value = line.split(' ')[1].replace('\n', '')
        data_dict[key] = value

with open("./data/fit_label_encoder.txt", 'r') as f:
    for line in f:
        key = line.split(' ')[0]
        value = line.split(' ')[1].replace('\n', '')
        label_dict[key] = value

trainset = []
files = glob.glob("./train/*.txt")
for file in files:
    f = open(file, 'r')
    each = []
    for row in f:
        each.append([row.split(' ')[0], row.split(' ')[1].replace('\n', '')])
    trainset.append(each)

traindata, trainlabel, X_train, Y_train = [], [], [], []
for each in trainset:
    data, label = [], []
    for row in each:
        data.append([x for x in row[0].split(',')])
        label.append([x for x in row[1].split(',')])
    traindata.append(np.array(data))
    trainlabel.append(np.array(label))

for i in range(len(traindata)):
    X_train.append([])
    Y_train.append([])
    for j in range(len(traindata[i])):
        X_train[i].extend(traindata[i][j])
        Y_train[i].extend(trainlabel[i][j])
    X_train[i] = np.array(X_train[i])
    Y_train[i] = np.array(Y_train[i])

max_length = 3466
# pad sequences to the same length
X_train = sequence.pad_sequences(X_train, maxlen = max_length)
Y_train = sequence.pad_sequences(Y_train, maxlen = max_length)
Y_train = np.array([y for each in Y_train for y in each])

le = LabelEncoder()
le.fit(Y_train)
encoded_Y = le.transform(Y_train)
encoded_Y = np_utils.to_categorical(encoded_Y) # 2D
encoded_Y = np.array([encoded_Y[i : i + max_length] for i in range(0, len(encoded_Y), max_length)])

dim_emb = 100
nb_data = 36649 # in total # 26785 in train # n + 1

# use word2vec for embedding layer
w2v_model = gensim.models.Word2Vec.load("./word2vec_100.model")
emb_weight = np.zeros((nb_data, dim_emb))
for i, data in data_dict.items():
    if data in w2v_model:
        emb_weight[int(i), :] = w2v_model[data]

# build model
model = Sequential()
model.add(Embedding(nb_data, dim_emb, weights = [emb_weight], input_length = max_length, mask_zero = True))
model.add(SimpleRNN(128, dropout = 0.1, recurrent_dropout = 0.1, return_sequences = True))
# add TimeDistributed layer to get labels for each document at each time slice
# softmax activation for multilabel classification
model.add(TimeDistributed(Dense(encoded_Y.shape[2], activation = 'softmax')))
# Nadam optimizer is better than RMSprop
nadam = Nadam(lr = 0.002, beta_1 = 0.9, beta_2 = 0.999, epsilon = None, schedule_decay = 0.004)
rmsprop = RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
# use cross entropy loss
model.compile(loss = 'categorical_crossentropy', optimizer = nadam)
print(model.summary())
print(model.get_config())
# epoch 40 - loss : 0.1000
model.fit(X_train, encoded_Y, epochs = 50, batch_size = 32, verbose = 2)

# save model
with open('./rnn-model.json', 'w') as f:
    f.write(model.to_json())
model.save_weights('./rnn-model.h5')

'''
TEST
'''
testset = []
# preprocessed and encoded same as trainset
test_files = glob.glob("./test/*.txt")
for file in test_files:
    f = open(file, 'r')
    each = []
    for row in f:
        each.append([row.replace('\n', '')])
    testset.append(each)

testdata, X_test = [], []
for each in testset:
    data = []
    for row in each:
        data.append([int(x) if x else 0 for x in row[0].split(',')])
    testdata.append(data)
testdata = np.array(testdata)

for i in range(len(testdata)):
    X_test.append([])
    for j in range(len(testdata[i])):
        X_test[i].extend(testdata[i][j])
    X_test[i] = np.array(X_test[i])

X_test = sequence.pad_sequences(X_test, maxlen = max_length)

# load model
model_json = open('./rnn-model.json', 'r').read()
load_model = model_from_json(model_json)
load_model.load_weights('./rnn-model.h5')
load_model.compile(loss = 'categorical_crossentropy', optimizer = nadam)

# make prediction
predict = load_model.predict(X_test)
predict = np.array([[[round(x) for x in doc] for doc in each] for each in predict])
print(predict.shape)
predict = [x.argmax(1) for x in predict] # for each file
predict = [list(set(x)) for x in predict]
predict = [[x for x in each if x != 0 and x != 1] for each in predict]

'''
FORMAT
'''
for i in range(len(test_files)):
    prediction = []
    for each in predict[i]:
        prediction.append(label_dict[str(each)][2:]) # remove I-
    f = open("./output/" + test_files[i][-10:-4] + ".xml", 'w')
    # write to xml
    f.write("<?xml version='1.0' encoding='UTF-8'?>\n")
    f.write("<root>\n")
    f.write("\t<TAGS>\n")
    for label in prediction:
        label = label.split('&')
        tag = label[0]
        attr = label[1].replace('_', ' ')
        if len(label) == 2: # SMOKER or FAMILY_HIST
            if tag == 'smoker':
                # attr is status
                f.write('\t\t<' + tag + ' status="' + attr + '"/>\n')
            if tag == 'family_hist':
                # attr is indicator
                f.write('\t\t<' + tag + 'indicator="' + attr + '"/>\n')
        if len(label) == 3:
            time = label[2].replace('_', '')
            if time == 'continuing':
                if tag == 'medication':
                    tag = tag.upper()
                    # attr is type1
                    f.write('\t\t<' + tag + ' time="before DCT" type1="' + attr + '" type2=""/>\n')
                    f.write('\t\t<' + tag + ' time="during DCT" type1="' + attr + '" type2=""/>\n')
                    f.write('\t\t<' + tag + ' time="after DCT" type1="' + attr + '" type2=""/>\n')
                else:
                    tag = tag.upper()
                    # attr is indicator
                    f.write('\t\t<' + tag + ' time="before DCT" indicator="' + attr + '"/>\n')
                    f.write('\t\t<' + tag + ' time="during DCT" indicator="' + attr + '"/>\n')
                    f.write('\t\t<' + tag + ' time="after DCT" indicator="' + attr + '"/>\n')
            else:
                if tag == 'medication':
                    f.write('\t\t<' + tag + ' time="' + time + '" type1="' + attr + '" type2=""/>\n')
                else:
                    f.write('\t\t<' + tag + ' time="' + time + '" indicator="' + attr + '"/>\n')

    # fill indicators that are not in labels
    if 'family_hist&present' not in prediction:
        f.write('\t\t<FAMILY_HIST indicator="not present"/>\n')
    if 'smoker&current' not in prediction and 'smoker&past' not in prediction and 'smoker&ever' not in prediction and 'smoker&never' not in prediction:
        f.write('\t\t<SMOKER status="unknown"/>\n')

    f.write('\t</TAGS>\n')
    f.write('</root>\n')
    f.close()
